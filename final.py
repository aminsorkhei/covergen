__author__ = 'sorkhei'

import os
import subprocess
import shutil
import lemmagen
from lemmagen.lemmatizer import Lemmatizer
import re
from wordcloud import WordCloud
from read_data import dataReader
import numpy as np
import random
import time
import flickr
from PIL import Image, ImageDraw
import pickle
import cStringIO
import urllib


class theme_grabber():
    parent_dir = os.getcwd()

    def __init__(self):

        self.mallet_address = os.path.join(self.parent_dir, 'mallet-2.0.7')
        self.mallet_output_address = os.path.join(self.parent_dir, 'mallet_output')
        self.selected_urls = []
        self.novel_chunk_dic = pickle.load(open('novel_chunk_dic.pkl'))
        self.num_of_topics = int(open('log').read().strip())
        print 'last time you run the model with %d themes ' % self.num_of_topics
        self.data_reader = dataReader(self.num_of_topics, self.mallet_output_address)
        self.word_id_dic, self.id_word_dic = self.data_reader.word_id_dic_generator()
        print '\nword_dictionary loaded'
        self.word_topic_weight_matrix = self.data_reader.normalizer()
        print '\nword_topic_weight_matrix loaded'
        self.doc_topic_matrix = self.data_reader.doc_topic_dic_generator()
        print '\ndoc_topic_matrix loaded and number of topics found '

    def train_model(self, _num_of_topics, _chunk_size):
        self.num_of_topics = _num_of_topics
        self.novel_chunk_dic = self.chunk_corpus(os.path.join(self.parent_dir, 'taggedCorpus'), _chunk_size)
        self.gather_nouns()
        self.run_lda(_num_of_topics)
        self.data_reader = dataReader(self.num_of_topics, self.mallet_output_address)
        self.word_id_dic, self.id_word_dic = self.data_reader.word_id_dic_generator()
        self.word_topic_weight_matrix = self.data_reader.normalizer()
        self.doc_topic_matrix = self.data_reader.doc_topic_dic_generator()
        self.weighted_display(25)




    def chunk_corpus(self, corpus_dir, chunk_size):
        """
        :param corpus_dir: directory of the corpus to be chunked
        :return: a novel_chunk_dic, chunks are written into a file called chunked_corpus.txt
        """

        res = open('chunked_corpus.txt', 'w')
        novels = [novel for novel in os.listdir(corpus_dir) if novel.endswith('txt')]
        novels = sorted(novels)
        novels_path = [os.path.join(corpus_dir, novel_dir) for novel_dir in novels]
        novel_chunk_dic = dict([(novel, []) for novel in novels])
        chunk_id = 0
        for novel in novels:
            novel_path = os.path.join(corpus_dir, novel)
            words = open(novel_path).read().split()
            chunk_count = 0
            chunked_words = []
            for word in words:
                if chunk_count < chunk_size:
                    chunked_words.append(word)
                    chunk_count += 1
                else:
                    # 1000 words gathered
                    res.write(' '.join(chunked_words) + '\n')
                    novel_chunk_dic[novel].append(chunk_id)
                    chunk_id += 1
                    chunk_count = 0
                    chunked_words = []
        res.close()
        pickle.dump(novel_chunk_dic, open('novel_chunk_dic.pkl', 'w'))
        return novel_chunk_dic

    def run_lda(self, _num_of_topics):
        self._num_of_topics = _num_of_topics
        devnull = open(os.devnull, 'wb')
        training_command = '%s/bin/mallet import-file  --input %s/final.txt ' \
                           '--output %s/train.mallet ' \
                           '--keep-sequence ' % (self.mallet_address, self.parent_dir, self.mallet_output_address)
        subprocess.call(training_command, stdout=devnull, shell=True)
        print 'Please wait while I am generating themes using LDA you have less than 4 minutes to go'
        start = time.time()
        execution_command = ' %s/bin/mallet train-topics ' \
                            '--input %s/train.mallet ' \
                            '--output-topic-keys %s/topic-keys.txt ' \
                            '--topic-word-weights-file %s/topic-word-weight.txt ' \
                            '--output-doc-topics %s/doc-topics.txt ' \
                            '--num-topics %d --num-top-words 50 ' \
                            % (self.mallet_address, self.mallet_output_address, self.mallet_output_address,
                               self.mallet_output_address, self.mallet_output_address, _num_of_topics)
        subprocess.call(execution_command, stdout=devnull, stderr=devnull, shell=True)
        end = time.time()
        print(num_of_topics)
        print 'it took only %d seconds ' % (end - start)
        print 'writing to log file'
        open('log', 'w').write(str(num_of_topics))

    def load_stop_words(self):
        """
        :return: a list of stop words of 5634 stop words of British English highly used in novels
        """
        text = open('stop.txt').read()
        words = text.split(',')
        stop_words = [word.strip() for word in words]
        return stop_words

    def gather_nouns(self):
        """
        Reads the chunk of input, segments that and writes the general nouns into the file
        in order to normalize the text, the function uses lemmatization
        :return: no return object, the result is written in a file called final.txt
        lda will run on this file
        """
        p = re.compile(ur'[a-zA-Z]+')
        lemmatizer = Lemmatizer(dictionary=lemmagen.DICTIONARY_ENGLISH)
        stop_words = self.load_stop_words()
        chunks = open('chunked_corpus.txt', 'r').readlines()
        final_result = open('final.txt', 'w')
        for chunk in chunks:
            chunk = chunk.strip().split(' ')
            nouns = [word.split('/')[0].lower() for word in chunk if word.split('/')[1] in ['NN', 'NNS']]
            processed_nouns = []
            for noun in nouns:
                processed_nouns += re.findall(p, noun)
            final_nouns = [lemmatizer.lemmatize(noun) for noun in processed_nouns
                           if lemmatizer.lemmatize(noun) not in stop_words]
            final_result.write(' '.join(final_nouns) + '\n')


    @staticmethod
    def return_novels():
        novels = os.listdir(os.path.join(theme_grabber.parent_dir, 'corpus'))
        novels = [novel.split('.')[0] for novel in novels if novel.endswith('.txt')]
        return novels



    def weighted_display(self, _num_of_top_words):
        print 'Now I am trying to generate word clouds for themes! stay tuned'
        figure_path = os.path.join(self.parent_dir, 'theme_figures')
        if 'theme_figures' in os.listdir(self.parent_dir):
            try:
                shutil.rmtree(figure_path)
            except Exception, e:
                print 'error occurred, please manually remove theme_figure directory'
                print e
        os.mkdir(figure_path)

        for topic_id in xrange(self.num_of_topics):
            top_words_id = np.argsort(self.word_topic_weight_matrix[:, topic_id])[::-1][0:_num_of_top_words]
            top_words_values = self.word_topic_weight_matrix[:, topic_id][top_words_id]
            real_words = [self.id_word_dic[word] for word in top_words_id]
            words = zip(real_words, top_words_values)
            wordcloud = WordCloud('font1.ttf', 1200, 500)
            wordcloud.fit_words(words)
            wordcloud.to_file(os.path.join(figure_path, 'theme ' + str(topic_id) + '.png'))

    def generate_cover(self, novel):
        cover_image = Image.new('RGB', (2000, 1600), 'black')
        # chunks_id = self.novel_chunk_dic[novel]
        chunks_id = self.novel_chunk_dic[novel + '.txt']
        matrix = np.array(self.doc_topic_matrix[chunks_id])
        sum_matrix = np.sum(matrix, 0)
        selected_topics = np.argsort(sum_matrix)[::-1][0:5]
        print 'These are the top themes in this novel'
        print selected_topics
        values = sum_matrix[selected_topics]
        values /= (np.sum(values))
        scale = 1 - values[0]
        topic_values = zip(selected_topics, values + scale)
        topic_images = []
        for topic_value_tuple in topic_values:
            image = self.generate_topic_image(topic_value_tuple[0])
            image = image.resize(tuple([int((item * topic_value_tuple[1])) for item in image.size]))
            topic_images.append(image)

        for index in xrange(len(topic_images)):
            if index == 0:
                cover_image.paste(topic_images[0], (0, 0))
            elif index == 1:
                cover_image.paste(topic_images[1], (1000, 0))
            elif index == 2:
                cover_image.paste(topic_images[2], (0, 801))
            elif index == 3:
                cover_image.paste(topic_images[3], (1001, 801))
            else:
                cover_image.paste(topic_images[4], (600, 400))
        cover_image.show()
        cover_image.save('main.png')

    def generate_topic_image(self, topic):
        """
        :param topic: topic number for which an image is going to be generated
        :return: return an Image object containing 4 photos
        """
        top_words_ids = np.argsort(self.word_topic_weight_matrix[:, topic])[::-1][0:5]
        top_words = [self.id_word_dic[word] for word in top_words_ids]
        values = self.word_topic_weight_matrix[:, topic][top_words_ids]
        values /= (np.sum(values))
        scale = 1 - values[0]
        topic_values = zip(top_words, values + scale)
        print 'These are the top words in the theme, each tuple, contains a word and the ' \
              'scaled probability to resize the photo\n'
        print topic_values
        print

        main_image = Image.new('RGB', (600, 800), 'black')
        selected_images = []
        for topic_tuple in topic_values:
            result = None
            url = ''
            while result is None and url not in self.selected_urls:
                try:
                    photo = flickr.photos_search(tags=topic_tuple[0])
                    random.shuffle(photo)
                    url = photo[0].getURL(size='Medium', urlType='source')
                    result = url
                    self.selected_urls.append(url)
                except:
                    print 'sth wrong when I was catching a photo regarding %s. I will try to catch another one' % tuple[
                        0]
                    pass

            image = Image.open(cStringIO.StringIO(urllib.urlopen(url).read()))
            image = image.resize((280, 380))
            image = image.resize(tuple([int((item * topic_tuple[1])) for item in image.size]))
            selected_images.append(image)

        for index in xrange(len(selected_images)):
            if index == 0:
                main_image.paste(selected_images[0], (0, 0))
            elif index == 1:
                main_image.paste(selected_images[1], (301, 0))
            elif index == 2:
                main_image.paste(selected_images[2], (0, 401))
            elif index == 3:
                main_image.paste(selected_images[3], (301, 401))
            else:
                main_image.paste(selected_images[4], (150, 150))
        return main_image


if __name__ == '__main__':
    welcome_text = 'Hi\nThis is Cover Photo Generator\nThis application tries to ' \
                   'genetate cover photos for a set of novels' \
                   '\n' \
                   '=======commands=======\n' \
                   'quit: to exit the application\n' \
                   'novels: to show the list of available novels\n' \
                   'generate: in order to generate cover for a given novel'

    print welcome_text

    print 'Please note that sometimes Flickr get some problems, if you received an error regarding that, please try ' \
          'again'
    command = raw_input('Please enter the command: \n')
    while command != 'quit':
        if command == 'novels':
            print '-------'
            print '\n***** '.join(theme_grabber.return_novels())
            print '-------'
        if command == 'generate':
            print '\nLoading required files, this may take a few minutes\n' \
                  'while you are waiting you can find some cool images of the topic in ' \
                  'theme_figures directory'
            tg = theme_grabber()
            generate_command = '\nThe initial system contains 25 themes and chunk size is set to be 1000\n' \
                               'if you have run the application before, the last trained model will be used \n' \
                               'in order to generate cover photos. You have two options: \n' \
                               '\ngenerate cover: in order to use the last trained model\n' \
                               'train: in order to train a new model\n'
            print generate_command
            command = raw_input('Please enter the command, train/generate cover?: \n')
            if command == 'generate cover':
                novel_name = raw_input('Please enter the novel name: ')
                tg.generate_cover(novel_name.strip())
            elif command == 'train':
                chunk_size = int(raw_input('Enter the chunk size: '))
                num_of_topics = int(raw_input('Enter number of themes: '))
                print 'Please wait while I am training the model'
                tg.train_model(num_of_topics, chunk_size)
                print 'Model generated, take a look at the theme at theme_figures'
                print 'Try to generate cover photos using generate command'
        command = raw_input('waiting for your command: \n')



                











